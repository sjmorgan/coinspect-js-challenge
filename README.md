# CoInspect JS Challenge
------------------------

## Challenge
You will be required to build a carousel with these minimum requirements:

* 3+ Panels of any type of content
* Controls to go from one panel to the next
* Cannot use an existing carousel plugin

In addition to these you may want to consider:

* Controls to go to a specific panel
* Time based slide changes
* Animation/Transitions for smoother presentation
* A server which provides some data to the carousel over an API

This is to be a demonstration of your ability to organize and develop your own implementation.
There are no limitations other than the carousel being your own code. Be prepared to answer
questions on any build system, framework or other technical choices you make along the way.

## Show us what you got!

 Once you are finished please push your final project to a public git repo on a service of
 your choosing (github, gitlab, bitbucket, other), and provide us a link to review and ask
 questions as to your code.

## Ask away
If you have any questions for clarification or concerns feel free to ask. These will help us
understand how we can collaborate together.